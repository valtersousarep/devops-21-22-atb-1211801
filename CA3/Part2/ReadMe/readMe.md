## CA3 - Part 2 - Virtualization with Vagrant

### 1. You should use https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ as an initial solution

So for this we will clone the repository into our computer:
For this we will use the following command:

    $ git clone https://ValterSousa@bitbucket.org/atb/vagrant-multi-spring-tut-demo.git

### 2. Study the Vagrantfile and see how it is used to create and provision 2 VMs:

### 3. Copy this Vagrantfile to your repository (inside the folder for this assignment)

We copied the Vagrantfile we got from the the previous steps into our repository.

![vagrantFile](vagrantfile.png)

### 4.Update the Vagrantfile configuration so that it uses your own gradle version of the spring application

To do this we need to update the Vagrantfile so it clones our repository and uses our gradle version of the 
spring application. We need to change the directory aswell and add the permissions to the gradlew so we can
run the "gradlew clean build"

![cloning](cloning.png)

### 5. Check https://bitbucket.org/atb/tut-basic-gradle to see the changes necessary so that the spring application uses the H2 server in the db VM. Replicate the changes in your own version of the spring application.

The changes we need to make in our own gradle application are the following:

buiil.gradle: <p>
Changing the plugin to support node.js. <p>
Adding dependency to support war file for deploying to tomcat. <p>
Adding the assemble script to run the webpack in the frontend. <p>

![frontendVersion](frontendVersion.png)
![dependencies](dependencies.png)
![frontend](frontend.png)

Application Properties: <p>
Changing the context path, all the datasource information, database platform and h2 console settings.

![appProperties](appProperties.png)

ServletInitializer.java:

![servletInitializer](servletInitializer.png)

App.js: <p>
Changing the path in our componentDidMount.

![appjs](appjs.png)

After the changes lets do:

    $ vagrant up

Now let's check if both machines are running:

![running](running.png)

As we can see, both machines are up and running
Now let's see if everything is ok in each machine.
First the DB:
Let's start with the command:

    $ vagrant ssh db

Now that we are inside the machine let's check the processes running:

    $ ps -xa

![db](db.png)

As we can see, h2 is running as expected.
Let's see with the url if everything is working as expected:

So we will use the following url:

    http://localhost:8080/react-and-spring-data-basic-0.0.1-SNAPSHOT/h2-console

And we will change the JDBC URL for the one we have on our application properties to access the database:

![login](login.png)
![h2Console](h2Console.png)

As we can see everything is working as expected.

Now let's check the web machine:

    $ vagrant ssh web
    $ ps -xa

![web](web.png)

As we can see the web Machine is working fine.
Now let's check the url the same way we did with the db machine.

    $ http://localhost:8080/react-and-spring-data-basic-0.0.1-SNAPSHOT

![webConsole](webConsole.png)

And as we can see the frontend is working just fine.

## Implementing Alternative - Hyperv

### VirtualBox vs Hyperv

Hyper-V and Oracle VM VirtualBox can both be used to handle a businesses server virtualization needs, but they also have a few features that set them apart from each other.

Hyper-V offers high performance virtual machines, and can output a lot of power depending on the hardware that it is running on.  Additionally, since Hyper-V is a type 1 hypervisor, virtual machines are always running as long as the hardware is.  Hyper-V also integrates well with Windows infrastructures, and is simple to use once it has been implemented.

Oracle VM VirtualBox can run on several operating systems, including Windows, Linux, and MacOS.  In addition to running on multiple host operating systems, Oracle VM VirutalBox can also create VirtualMachines using multiple guest operating systems, rather than just Windows. Lastly, implementation of Oracle VM VirtualBox is simple, most businesses will be able to just install the hypervisor and be ready to go.

![types](types.webp)

### Implementation

The first thing to do is to download and install hyperv manager. <p>

After this step is completed, we need to update our vagrant file so it works with hyperV <p>
We need to change the provider:<p>

![provider](provider.png)

We also need to change the virutal machine box we will use. We need a box that is compatible with hyperv provider.<p>
For example bento/ubuntu-16.04.

![boxes](boxes.png)

With hyperv, the IP adresses are set by hyperv itself, we cannot configure the ip adresses we want to use.<p>
For that reason we can comment those line:

![ips](ips.png)

The last thing we need to do is check if hyperv is active in windows system:<p>

![system](system.png)

Now finally we can use the vagrant file to create the Virtual Machines using "hyperv" provider.<p>
To do this with hyperv, we need to run the command line as administrator.<p>
Now we will run the following command to force the use of the provider hyperv<p>

    $ vagrant up --provider=hyperv

![build](build.png)

And as we can see in the hyperv Manager, both machines are up and running:<p>
![manager](manager.png)

We can also check vagrant status:<p>
![status](status.png)
![vagrantDB](vagrantDB.png)
![vagrantWEB](vagrantWEB.png)

Now let's check the ip adresses and see if the app is working properly.<p>
Because hyperv set's the ip addresses on its own, we need first to check the ip address that was assigned to the 
database and change it in application.properties spring.datasource.url:<p>
![ifconfig](ifconfig.png)
![ipChange](ipChange.png)

Now let's check the ip address that was assigned to the Web part of the application and check if everything is running correctly:<p>
![webAddress](webAddress.png)

We had an issue with a "404 error page not found". <p>
After several different attempts to solve the problem, we ran the command 

    $ ./gradlew clean build

Even thou this command was already in the vagrantFile to be ran, this solved the problem and everyhting started working properly.


![h2FrontEnd](h2FrontEnd.png)
![webFrontEnd](webFrontEnd.png)


