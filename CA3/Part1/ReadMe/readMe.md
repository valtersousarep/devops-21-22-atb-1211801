## CA3 - Part 1 - Virtualization with Vagrant

### 1. You should start by creating your VM as described in the lecture

So first we need to install the Oracle Virtual Box from the following website https://www.virtualbox.org/wiki/Downloads

After we installed the Oracle Virtual Box, we can create a new Virtual Machine and after we configure all the necessary steps 
we will have something like this:

![createVM](createVM.png)

This are the properties of our VM with the name "ubuntu-devops-valter", OS Linux Ubuntu 64.

Now we need to install an OS so we can work on the VM. So we will install Ubuntu and install some packages and softwres following
the lecture and instructions on the teachers slides.

### 2. You should clone your individual repository inside the VM

To clone the repository we need to enter our VM and use the following command:

    $ git clone https://ValterSousa@bitbucket.org/valtersousarep/devops-21-22-atb-1211801.git

Or access our VM remotely by our host command line using the following command:

    $ ssh valter@192.168.56.5 

And when inside, use the previous command.

### 3. You should try to build and execute the spring boot tutorial basic project and the gradle_basic_demo project 

To run the Maven project we will do the following steps:

So in our Virtual Machine, we need to access the folder with the mvnw file. so we need to:

    $ cd devops-21-22-atb-1211801/CA2/Part 2/tut-react-and-spring-data-rest/basic

And then run the following command to execute the spring project:

    $ ./mvnw spring-boot:run

An error occurs stating that we don't have the permission to do this, so we need to add that permission first and then run the previous command

    $ sudo chmod u+x mvnw

And we have the project running:

![startSpring](startSpring.png)

We can check the app running using our IP localhost adress:

![maven](maven.png)

Now to run the gradle Project we need to do as following:

Go to the folder where we can start gradlew:

    $ cd devops-21-22-atb-1211801/CA2/Part 1/Gradle/gradle_basic_demo

And then run the following command to build the gradle project:

    $ ./ gradlew build

![gradleBuild](gradleBuild.png)

Now lets run the application with the following command:

    $ ./ gradlew runServer

![runServer](runServer.png)

The chat Server is now up and running.

### 4. For web projects you should access the web applications from the browser in your host machine

### 5. For projects such as the simple chat application you should execute the server inside the VM and the clients in your host machine. Why is this required?

We already have the server running in the VM.
But because the OS we are running in the VM does not have graphic display, we have to run the client on the "host"
side. If we try to run the client on the VM side we will get an error.

![runClient](runClient.png)

To do this we must create a new task in gradle to run the client with the VM's server ip on the host side:

    task runClientVM(type:JavaExec, dependsOn: classes){
        group = "DevOps"
        description = "Launches a chat client that connects to a server on 192.168.56.5 "

        classpath = sourceSets.main.runtimeClasspath

        main = 'basic_demo.ChatClientApp'

        args '192.168.56.5', '59001'
    }

After we add this task we go to the windows command prompt and run the following command from the gradle folder:

    $ gradlew.bat runClientVM

We open two chats to check if things are working properly. We also can see on the VM's server that the users
are joining the chat:

![chats](chats.png)

Regarding the other tasks:

Running the tests using the clean task:

For this we need to run the command:

    $ ./gradlew clean test

And then we can see the tasks that were ran correctly:

![cleanTest](cleanTest.png)

Now let's check the "make a backup":

As we can see there is no backup folder:

![ls](ls.png)

Now let's run the task and see if the folder is created with the backup inside:

![backup](backup.png)

Now let's cheack the making a zip file task.
As we can see there is no folder "backupZIP"

![backupZip](backupZip.png)

So let's run the task and see what happens:

![zip](zip.png)

As we can see the backup of the src file was created inside a folder named backupZip as pretended.