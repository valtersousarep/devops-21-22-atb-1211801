# <b>Track 2 - Gradle, Local deployment of Web + DB (*H2*) to Vagrant virtual machines with Ansible</b>


## <b>INTRODUCTION</b>

In this assignment we will use a Vagrant file to create 3 different Virtual Machines, web, db and control.

Then we will use the control machine to deploy our application in the web machine and the database in the db machine using Jenkins and Ansible.

In this report we will talk about the main issues we encountered and all the files we need from Vagrant to Jenkins Files and also Ansible Playbooks.


## <b>IMPLEMENTATION</b>

### <b>1- Build virtual machines with Vagrant</b>

The first step will be to create 3 Virtual Machines:
    -Web
    -DB
    -Control (This machine will have Jenkins and Ansbile)

For this we need a Vagrantfile and the Oracle Vagrantbox provider to run it.

We need a box that works with Vagrantbox, so after some research on the website https://app.vagrantup.com/boxes/search, we chose "bento/ubuntu-20.04" (Focal Fossa), because it was thelatest long-term support (LTS) version of Ubuntu.

##### <b>1.1- Create Vagrantfile</b>

We installed python in all three machines because we need it as an agent to run Ansible.

We also installed openjdk-11-jdk because it is the version of Java in our project.

In the "web" and "db" machines we have not installed anything else in particular because those configurations will be done later using Jenkins and Ansible through the "Control" machine.

For this we needed to install in the "Control" machine, Jenkins, Ansible and NodeJS.


    Vagrant.configure("2") do |config|
    config.vm.box = "bento/ubuntu-20.04"
    config.vm.provision "shell", inline: <<-SHELL
      sudo apt-get update -y
      sudo apt-get install iputils-ping -y
      sudo apt-get install python3 --yes
      sudo apt-get install openjdk-11-jdk-headless -y
    SHELL


    ## web == 192.168.33.10

    config.vm.define "web" do |web|
      web.vm.box = "bento/ubuntu-20.04"
      web.vm.hostname = "web"
      web.vm.network "private_network", ip: "192.168.33.10"
      web.vm.provider "virtualbox" do |v|
        v.memory = 1024
      end
      # To access tomcat from the host using port 8080
      web.vm.network "forwarded_port", guest: 8080, host: 8080
      web.vm.provision "shell", inline: <<-SHELL, privileged: false
      SHELL
    end
  

    ### db == 192.168.33.11

    config.vm.define "db" do |db|
      db.vm.box = "bento/ubuntu-20.04"
      db.vm.hostname = "db"
      db.vm.network "private_network", ip: "192.168.33.11"
      # To access H2 console from the host using port 8082
      db.vm.network "forwarded_port", guest: 8082, host: 8082
      # To connect to the H2 server using port 9092
      db.vm.network "forwarded_port", guest: 9092, host: 9092
    end
  

    ## control == 192.168.33.12

    config.vm.define "control" do |control|
      control.vm.box = "bento/ubuntu-20.04"
      control.vm.hostname = "control"
      control.vm.network "private_network", ip: "192.168.33.12"
      control.vm.provider "virtualbox" do |v2|
        v2.memory = 2048
      end

      # To run ansible from jenkins
      control.vm.synced_folder ".", "/vagrant", mount_options: ["dmode=775,fmode=600"]

      # To access jenkins in port 8081
      control.vm.network "forwarded_port", guest: 8080, host: 8081
      control.vm.provision "shell", inline: <<-SHELL
      
      sudo apt-get update -y
      sudo apt-get install -y curl
      sudo apt-get install apt-transport-https
      sudo apt-get install -y nano git
      sudo apt-get install -y --no-install-recommends apt-utils
      sudo apt-get install software-properties-common --yes
      
      curl -sL https://deb.nodesource.com/setup_12.x -o nodesource_setup.sh
      sudo bash nodesource_setup.sh
      sudo apt install nodejs

      # Install Ansible
      sudo apt-add-repository --yes --update ppa:ansible/ansible
      sudo apt-get install ansible --yes

      # Install Jenkins
      sudo apt-get install -y avahi-daemon libnss-mdns
      sudo apt-get install -y unzip
      wget https://mirrors.jenkins.io/war-stable/latest/jenkins.war
      sudo java -jar jenkins.war
      
      SHELL
    end

    end

<b>Important:</b> to install Jenkins, we got the jenkins.war file and executed it in the "control provision" - as a super user. This way, we prevented 'Host key verification failed' error in Jenkins pipeline.

![](./1-hostKeyError.png)

Now with the VM's up and running let's configure Jenkins.

### <b>2- Configure Jenkins</b>

#### <b>2.1- Post-installation setup</b>

##### <b>2.1.1- Unlock Jenkins</b>

To start Jenkins first we need a password given to us on the installation of Jenkins and use it to have administrator permissions.

![](./4-post-installation-wizard.png)

#### <b>2.2- Customize Jenkins with plugins</b>

Now we can customize Jenkins and install the plugins we want.
We select the option "Install Suggested Plugins".

![](./2-jenkinsCustomize.png)
![](./5-pluginsInstallation.png)

#### <b>2.3- Create user *'devops'*</b>

Creation of the first admin user on Jenkins.
It is not a mandatory step, we can use the default credentials but we chose to select our own.

    Username: Devops
    Email: 1211801@isep.ipp.pt

![](./6-createUser.png)

#### <b>2.4- Create the Jenkinsfile</b>

    pipeline {
    agent any

    stages {
        stage('Check out') {
            steps {
                echo 'CHECKING OUT...'
                git credentialsId: 'ForkRepository', url: 'https://bitbucket.org/switch-2021/projectg5-devops/'
            }
        }
        stage('Change application.properties file') {
            steps {
                echo 'CHANGING APPLICATION PROPERTIES FILE...'
                sh 'sudo rm src/main/resources/application.properties'
                sh 'sudo cp DEVOPS/task2/application.properties src/main/resources/'
            }
        }
	  stage('Change URL_API file') {
            steps {
                echo 'CHANGING URL_API FILE...'
                sh 'sudo rm webapp/src/services/URL_API.js'
                sh 'sudo cp DEVOPS/task2/URL_API.js webapp/src/services'
            }
        }
        stage('Setup servers') {
            steps {
                echo 'SETTING UP SERVERS...'
                ansiblePlaybook credentialsId: 'control', disableHostKeyChecking: true, inventory: 'DEVOPS/task2/hosts', playbook: 'DEVOPS/task2/playbook-config.yaml'
            }
        }
        stage('Assemble') {
            steps {
                echo 'ASSEMBLING...'
                sh './gradlew assemble'
            }
        }
        stage('Report') {
            steps {
                echo 'REPORTING...'
                sh './gradlew test'
                junit testResults:'build/test-results/test/*'
            }
        }
        stage('Archive') {
            steps {
                echo 'ARCHIVING...'
                archiveArtifacts 'build/libs/*.war'
            }
        }
        stage('Build') {
            steps {
                echo 'BUILDING FRONTEND...'
                dir('webapp') {
                    sh 'npm install'
                    sh 'sudo npm run build'
                }
            }
        }
        stage('Deploy') {
            steps {
                echo 'DEPLOYING...'
                ansiblePlaybook credentialsId: 'control', disableHostKeyChecking: true, inventory: 'DEVOPS/task2/hosts', playbook: 'DEVOPS/task2/playbook-run.yaml'
            }
        }
    }
    }

#### <b>2.5- Create job *'task2'*</b>

Now we are able to create the pipeline with our Jenkinsfile.
The name of the pipeline is "task2" and once created we can configure the pipeline and use our Jenkinsfile with it.

![](./9-pipelineCreation.png)

#### <b>2.5.1 - Creating credentials</b>

Configuring the pipeline, because we want to clone our repository with the gradle application, we needed to create an app password on bitbucket and then create a global password on jenkins.

![](./8-appPassword.png)
![](./10-globalCredentials.png)

With this passwords created we can now use them to clone our repository using the pipeline.

![](./11-repositoryClone.png)

#### <b>2.5.2- Jenkinsfile Path</b>

![](./12-jenkinsFilePath.png)

#### <b>2.5.3- Install plugins</b>

We also needed to install the following plugins for some of the stages of the Jenkinsfile:

- HTML Plubisher plugin 1.30 (to publish the reports of stage Report that runs the tests)
- Ansible plugin 1.1. (to run Ansible playbooks)


### <b>3- Automate tasks with Ansible</b>

#### <b>3.1- Create configuration source file (ansible.cfg)</b>

With this file we are declaring that ansible uses as inventory the file in /vagrant/hosts and as user the name "vagrant" which will be needed ahead.

    [defaults]
    inventory = /vagrant/hosts
    remote_user = vagrant

#### <b>3.2- Create inventory file (hosts)</b>

    [servers]

    web ansible_ssh_host=192.168.33.10 ansible_ssh_port=22 ansible_ssh_user=vagrant ansible_ssh_private_key_file=/vagrant/.vagrant/machines/web/virtualbox/private_key

    db ansible_ssh_host=192.168.33.11 ansible_ssh_port=22 ansible_ssh_user=vagrant ansible_ssh_private_key_file=/vagrant/.vagrant/machines/db/virtualbox/private_key

In this file we define a group called "servers" and inside that group we have two different variables called web and db like the virtual machines we created

For this two variables we declare:

<b>ansible_host</b>
The name of the host to connect to, if different from the alias you wish to give to it.

<b>ansible_ssh_host</b>
The ip of the host to connect to, if different from the alias you wish to give to it.

<b>ansible_port</b>
The connection port number, if not the default (22 for ssh)

<b>ansible_user</b>
The user name to use when connecting to the host

<b>Ansible_ssh_private_key_file</b>
Private key file used by ssh. Useful if using multiple keys and you don’t want to use SSH agent.


#### <b>3.3- Create playbooks</b>

We created two different playbooks:<p>
    - playbook-config.yaml 
    - playbook-run.yaml

#### <b>3.3.1- Configure virtual machines: playbook-config.yaml</b>

    ---
    - hosts: servers
    become: yes
    tasks:
    - name: Update cache
      apt: update_cache=yes
    - name: Remove Apache
      apt: name=apache2 state=absent
    - name: Update cache
      apt: update_cache=yes

    - hosts: db
    become: yes
    tasks:
    - name: Install H2
      get_url:      
        url: https://search.maven.org/remotecontent?filepath=com/h2database/h2/2.1.214/h2-2.1.214.jar
        dest: ./
    - name: Initialize H2
      ansible.builtin.shell:   
        java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
      args:
        chdir: ./

    - hosts: web
     become: yes
    tasks:
    - name: Install Node.js 12.18.3
      ansible.builtin.shell: |
        curl -sL https://deb.nodesource.com/setup_12.x -o nodesource_setup.sh
        sudo bash nodesource_setup.sh
        sudo apt install nodejs
    - name: Install Tomcat 9
      apt: name=tomcat9 state=present
    - name: Install Apache
      apt: name=apache2 state=present
    - name: Clean Tomcat
      ansible.builtin.file:
        path: /var/lib/tomcat9/webapps/*
        state: absent

In this playbook we define the dependencies and software we need on both our machines in order to run our application.

Things like apache for the frontend, tomcat for the backend, and node were specificly installed in the web machine.

In the db machine we installed git, the H2 database to run our database and also a command to initialize it.

For both machines under the name "servers", we remove any apache or tomcat that already exist before we install the new ones, mainly for safety reasons.

Different versions of Apache Tomcat are available for different versions of the specifications. Apache Tomcat 9.0. x requires Java 8 or later - we use Java 11.

![](./13-tomcat.png)
(font: https://tomcat.apache.org/whichversion.html)

#### <b>3.3.2- Deploy Web and DB: playbook-run.yaml</b>

    ---
    - hosts: servers
    become: yes
    tasks:
    - name: Update cache
      apt: update_cache=yes

    - hosts: web
    become: yes
    tasks:
    - name: Stop Tomcat Service
      ansible.builtin.shell: sudo service tomcat9 stop
    - name: Copy war file from localhost
      copy:
        src: ~/.jenkins/workspace/test/build/libs/switchproject-1.0-SNAPSHOT.war
        dest: /var/lib/tomcat9/webapps/
    - name: Start Tomcat Service
      ansible.builtin.shell: sudo service tomcat9 start
    - name: Stop Apache Service
      ansible.builtin.shell: sudo service apache2 stop
    - name: Clean frontend
      ansible.builtin.file:
        path: /var/www/html/*
        state: absent
    - name: Copy frontend build files
      copy:
        src: ~/.jenkins/workspace/test/webapp/build/
        dest: /var/www/html/
    - name: Start Apache Service
      ansible.builtin.shell: sudo service apache2 start


In this playbook we declare tasks only for the web VM.

We copy the war file from the localhost in order to build our application in the VM and we also run a command to start tomcat.

We also copy the frontend build files in order to build the frontend in our VM.

### <b>3.4- Configure H2 to connect with DB server</b>

#### <b>3.4.1- Setup application.properties in our Java project</b>

    spring.datasource.driverClassName=org.h2.Driver

    spring.datasource.username=sa

    spring.datasource.password=password

    spring.jpa.database-platform=org.hibernate.dialect.H2Dialect

    spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;CACHE_SIZE=8192;DB_CLOSE_ON_EXIT=FALSE

    spring.jpa.hibernate.ddl-auto=create-drop

    load.bootstrapData=true

### <b>4- Build with Jenkins</b>

In this step we had several problems when we reached the first stage using ansible.
The problems were usually related to "unreachable hosts", sometimes web, sometimes db, sometimes in "gathering facts" stage, sometimes in other stages.
After a couple of days of trying different sollutions, eventually i tried to build the VM's in a different computer with more RAM.
And everything went well, so we had to change computer, i used my colleague from Group 5 Daniel computer, and do everything again from scratch on his machine.
This time things worked successfully as we can see in the below prints.

![](./16-pipelineSuccess.jpg)
![](./17-pipelineSuccessTwo.jpg)

Here we see the frontend working.

![](./14-appWorking.jpg)

And the frontend receiving data from the database.

![](./15-appWorkingWithData.jpg)

### 5- Analysis of the alternatives

#### 5.1- Comparison Gradle vs Maven

![](./18-Maven_Gradle.jpg)

### Regarding Building and extension of new functionalities

#### Gradle

- Gradle is based on a graph of task dependencies in which tasks are the things that do the work, while Maven is based on a fixed and linear model of phases.
- Gradle is also very flexible and based on a script. Custom builds would be easy to do on Gradle.

#### Maven

- With Maven, goals are attached to project phases, and goals serve a similar function to Gradle's tasks, being the “things that do the work".
- Plugin is a group of goals. Maven is based around the central concept of a Build Life Cycles. 
Inside each Build Life Cycles there are Build Phases, and inside each Build Phases there are Build Goals.
- With Maven, we can easily define our project’s metadata and dependencies, but creating a highly customized build might be a nightmare for Maven users. 
The POM file can easily get bloated as our project grows and might as well be an unreadable XML file later on.

### Regarding Performance

Performance-wise, both allow for multi-module builds to run in parallel. 
However, Gradle allows for incremental builds because it checks which tasks are updated or not. 
If it is, then the task is not executed, giving us a much shorter build time.

### Regarding Dependencies and directory structure

- Maven provides simple yet effective dependency management, and since it has a directory structure for projects,
we have some sort of standard layout for all our projects. 
It uses a declarative XML file for its POM file and has a host of plugins that we can use. 
- Gradle uses the directory structure we see on Maven, but this can be customized. 
It also uses the same GAV format that Maven uses to identify artifacts.

### Preference - Gradle

It is simples to create a customized build with gradle, the POM file with Maven get's really complex and bloated. This makes gradle build easier to read and simpler to customize. 

#### 5.2- Comparison Containers *vs* Virtual Machines

![](./19-VirtualMachinesVSContainers.png)

Containers and virtual machines are very similar resource virtualization technologies. Virtualization is the process in which a system singular resource like RAM, CPU, Disk, or Networking can be ‘virtualized’ and represented as multiple resources. The key differentiator between containers and virtual machines is that virtual machines virtualize an entire machine down to the hardware layers and containers only virtualize software layers above the operating system level.

In our final tasks we used both containers and VM's to deplyo our Maven and Gradle applications. Here are some pros and cons of both.

||Pros|Cons|
|--|--|--|
|<b>Virtual Machines</b>|- <b>Full isolation security</b>: virtual machines run in isolation as  a fully standalone system. This means that virtual machines are immune to any exploits or interference from other virtual machines on a shared host.<br>- <b>Interactive development</b>: containers are usually static definitions of the expected dependencies and configuration needed to run the container. Virtual machines are more dynamic and can be interactively developed. |- <b>Iteration speed</b>: virtual machines are time consuming to build and regenerate because they encompass a full stack system. Any modifications to a virtual machine snapshot can take significant time to regenerate and validate they behave as expected.<br>- <b>Storage size cost</b>: virtual machines can take up a lot of storage space. They can quickly grow to several gigabytes in size.|
|<b>Containers</b>|- <b>Iteration speed</b>: because containers are lightweight and only include high level software, they are very fast to modify and iterate on. <br>- <b>Robust ecosystem</b>: most container runtime systems offer a hosted public repository of pre-made containers. These container repositories contain many popular software applications, saving time for development teams.|- <b>Shared host exploits</b>: containers all share the same underlying hardware system below the operating system layer, it is possible that an exploit in one container could break out of the container and affect the shared hardware. There is a security risk in using one of these public images as they may contain exploits or may be vulnerable to being hijacked by nefarious actors.|

In our tasks i worked more with VM's than Dockers. With that said i really like working with VM's, there is a bad side though, using VM's consumes a lot of computer resources and many times the machines are not built correctly because of that and demands of us patiente to build them again and again. 
From the feedback i had on Dockers, this process is smoother and faster to modify and adapt.

#### 5.3- PostgreSQL *vs* H2 Database

<b>H2 Database</b>  is a relational database management system written in Java. It can be embedded in Java applications or run in client-server mode; 

<b>PostgreSQL</b> is an advanced object-relational database management system that supports an extended subset of the SQL standard, including transactions, foreign keys, subqueries, triggers, user-defined types and functions.

||Pros|Cons|
|--|--|--|
| H2 | <b>Can run</b> as an in-memory database.<br><b> Simple</b> and quick to get started with, and is light weight (only 2MB).<br> <b>SQL compliant</b> so it compatible with most relational database. | <b>H2 is not</b> yet a mature product. <br> <b>If raw</b> SQL queries are used there maybe be differences between MySQL & H2. ORM library should be used.<br> <b>Support</b> seems to be community-based only. 
|PostGreSQL| <b>The stability</b> it offers, its speed of response and its resource management is excellent even in complex database environments and with low-resource machines. <br> <b>The large</b> amount of resources it has in addition to the many own and third-party tools that are compatible that make productivity greatly increase.<br> <b>The adaptability</b> in various environments, whether distributed or not, [is a] complete set of configuration options which allows to greatly customize the work configuration according to the needs that are required. <br> <b>The excellent</b> handling of referential and transactional integrity, its internal security scheme, the ease with which we can create backups are some of the strengths that can be mentioned. | <b>The query</b> syntax for JSON fields is unwieldy when you start getting into complex queries with many joins. <br>

In the development of our tasks i only worked with H2 and also in our project i worked with H2 which does not give me enough information to have a preference. 

## Final considerations / Conclusion

This assingment taught us a lot of all the tehcnologies we used along the semestre, it was a lot of work and there were a l lot of problems to solve but it pays off in the end from all the knowledge we gain.

But we have to say that the main one was related to computer resources, my computer has 8GB RAM but it seems it was not enough, even shuting down unecesssary background programs working.

Creating VM's demands a lot of memory and resources from the computer.

The only solution was to use another computer with more resources and was interesting to see the difference, not only because the deployment was successfull but also because the speed of the whole process was way higher.
