## CA5, Part 1 - CI/CD Pipelines with Jenkins 

First step is to install Jenkins in our local machine. For this we need to go to "https://www.jenkins.io/download/"
and get the jenkins.war in order to install jenkins.<p>

Now in the command line prompt we need to run:

    $ java -jar jenkins.war --httpPort=9090 --enable-future-java

We needed to add --httpPort=9090 because our 8080 port is being used already and this way we avoid collisions. And because we had an error with the java version we added
--enable-future-java as well which solved the problem.

![jenkinsInstalled](jenkinsInstalled.png)

Now we need to go to the localhost:9090 and start the installation process.
We had a few issues with the installation of the plugins but after we finished the installation and entered jenkins,
we were able to manage jenkins plugins properly and install the remaining ones. After this step we rebooted the machine
and as we can see jenkins is up and running properly.

![jenkinsRunning](jenkinsRunning.png)

### 1. Creating a pipeline, edit and executing the script using Jenkins (without credentials)

To do this step lets go to jenkins and create a new item pipeline:<p>

![creatingPipeline](creatingPipeline.png)

And while creating the new pipeline with the name ca5_part1, we must write the script code and run the script. <p>
We used the script the teacher provided us. After a few tries and checking the console log of each build, we adpated 
our script to the following:

![script1](script1.png)
![script2](script2.png)

The changes we made were the following: <p>

* Changed the repository to clone for my repository;
* Added the Stage and Test stages with the correct "dir" and commands for each;
* Deleted the build stage;
* Because we are running windows and not linux we use "bat" and not "sh";
* In the test stage we added "junit 'build/test-results/test/*.xml'" in order to have a test report available when we build;

![build](build.png)
![jenkinsTests](jenkinsTests.png)

As we can see the build was successful and the tests were ran correctly and passed.
In summary, in the "checkout" stage we clone our repository in order to run the application in the "Assemble" stage.<p>
After this two steps we have the "test" stage where we run the tests for the application.<p>
And in last we have the "Archiving" stage where we archive the jar file that was created when the application is built.

### 2. Creating a pipeline, edit and executing the script using JenkinsFile (with creadentials)


#### JenkinsFile
For this part, we created a Jenkinsfile with the same code as the script we created before, but with a few changes: <p>

![jenkinsFile1](jenkinsFile1.png)
![jenkinsFile2](jenkinsFile2.png)

In this jenkilsFile we added the gitCredentialsID and used the "jenkinsKey" that we created as i will explain later.<p>
Because we had issues building this app with jenkins, after a lot of research and several different tries, the teacher 
gave the idea of creating a new repository with less used space since our original repository had 1.5GB.<p>
So we created "https://bitbucket.org/valtersousarep/valtersousa_ca5/src/master/" this repository and gave permissions for the teacher
to have access to. In this repository we only added the chat app in order to check if jenkins was building it.<p>
After we cloned this new repository the app was not building and we discovered we had .jar files in gitignore, so our wrapper
was not working, after deleting .jar file from gitignore the problem was solved.

#### Creating SSH keys

So for the jenkinsKey that we used in the jenkinsFile we had to create SSH keys, for this we went to our command prompt and used the following code:

    $ ssh-keygen

![sshKeygen](ssh-keygen.png)

Two keys were generated and we had to use them both, one on bitbucket and one on jenkins:

![addingSSHkey](addingSSHKey.png)
![sshKeyAddedBitbucket](sshKeyAddedBitbucket.png)
![jenkinsKeyCreated](jenkinsKeyCreatedJenkins.png)
![jenkinsKeyAdded](jenkinsKeyAddedJenkins.png)

#### Configuring the new pipeline

After this, we added the jenkinsKey we created in Bitbucket to our git CredentialsID in our jenkinsFile, so the file has the access to our repository in order to clone it: <p>
![jenkinsKey](jenkinsKey.png)

We also turned our repository private to check if the credentials were working properly.<p>

Also, we changed the repository to clone in the jenkinsFile has we explained before and changed the directory for each of the stage in compliance with the new repository. <p>
Now we copy this jenkinsFile into the app folder and push it into our new repository.<p>

Now we can create a new pipeline named ca2_part1 and configure it to use Pipeline Script from SCM using git, our repository and the jenkinsKey we created
in order to have access to our repository and use the jenkinsFile we pushed into the repository to build our application. <p>
We had some issues in saving the configuration of this pipeline and after some research we concluded that the problem was using Java17, so we deleted
Java17 from our machine and installed the newest version of Java11, also we changed the environment system variables JAVA_HOME in order to our
system recognising this version of Java and problem was solved. After this we no longer had to use --enable-future-java 
in order to start jenkins because now we are using the correct Java version to run Jenkins which is Java11.

![newPipeline1](newPipeline1.png)
![newPipeline2](newPipeline2.png)

And as we can see, the build was successful and the test report was created:

![buildSuccessful](buildSuccessfull.png)
![testResult](testResult.png)



