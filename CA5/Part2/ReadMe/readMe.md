## CA5, Part 2 - CI/CD Pipelines with Jenkins 

For the seconde part of the assignment we are once again using the second repository as the teacher suggested to prevent
the problem of oversize when cloning it. <p>
We are using this repository: https://bitbucket.org/valtersousarep/valtersousa_ca5/src/master/ <p>
We also had to install some plugins on jenkins in order to conclude this assignment, we will explain all of them during this report: <p>
![plugins](plugins.png)

In this part of the assignment we are creating a pipeline in Jenkins to build the spring boot application , gradle "basic"
version.

For this we need to create a Jenkinsfile with the stages suggested by the assignment: <p>
![jenkinsFile1](jenkinsFile1.png)
![jenkinsFile2](jenkinsFile2.png)
![jenkinsFile3](jenkinsFile3.png)

We used the same stages we had in Part1 and added two more: <p> 
* ***Javadoc*** : Generates the javadoc of the project and publish it in Jenkins. ( use publishHTML step to archive/publish html reports);
![javadoc](javadoc.png)
For this stage we had to install two plugins in Jenkins: "HTML Publisher" and "Javadoc" in order to achieve this report: <p>
![HTML](HTML.png)
![HTMLReport](HTMLReport.png)

* ***Publish Image*** : Generate a docker image with Tomcat and the war file and publish it in the Docker Hub.  
  Use docker.build to build an image from a Dockerfile in the same folder as the Jenkinsfile. The tag for the image will be the job build number of Jenkins.

![dockerImage](dockerImage.png)<p>
For this step we needed to create a Dockerfile in the application folder the same way we did with the Jenkins file, in order
to create an image with the Tomcat and the war file.<p>
![dockerFile](dockerFile.png)<p>
Also we created a key in order to access our Dockerhub and publish the image. We used this key in the "docker.withRegistry" part.<p>
![jenkinsDockerKey](jenkinsDockerKey.png)<p>
Then we used docker.build of our docker file and set the repository "ca5_part2" of our docker hub as the repository where we wanted the image to be published.<p>
![publish](publish.png)
For this stage we needed to install the plugins "Docker commons" and "Docker pipeline".<p>


After all of these steps were set, we configured our pipeline and tried to run it: <p>
![configuration1](configuration1.png)
![configuration2](configuration2.png)

When we tried to build, we had always the same issue ocurring in the last stage. <p>
After checking the logs we verified that the credentials for the docker hub were working correctly but then something happened and the build
was unsuccessful.
Eventually we found out the name of our folder had a space "Part 2", and even thou that worked in the stages before this one, because '' were used,
in this point with "" spaces won't work, so we changed the name of the folder and the build was successful.<p>

![buildLog](buildLog.png)
![build](build.png)

And as we can see everything went well. We also can see in the previous pictures that the javadoc HTML was created and the docker image was
created and published as expected. <p>

## Alternative (Buddy)

### Jenkins vs Buddy

What are the differences?

**Buddy** - Build, test and deploy on push in seconds. Git platform for web and software developers with Docker-based tools for Continuous Integration and Deployment.<p>

**Jenkins** - An extendable open source continuous integration server. In a nutshell Jenkins CI is the leading open-source continuous integration server. 
Built with Java, it provides over 300 plugins to support building and testing virtually any project.<p>

Buddy belongs to "Continuous Deployment" category of the tech stack, while Jenkins can be primarily classified under "Continuous Integration".<p>

![jenkinsVSbuddy](buddyVsJenkins.png)

#### Deployment

Buddy is a SaaS offering, meaning there is nothing to get running on your side. You simply click a couple of buttons and you're away.<p>
Jenkins comes packaged as a binary.  You'll need a platform on which to deploy it, such as an EC2 instance or a Kubernetes cluster. <p>

#### Maintenance

Jenkins servers have been known to crash and fail regularly and have required tuning in the past. This is often an experience issue, rather than a problem with the tool. A properly configured Jenkins instance will run without fault for as long as it needs to. <p>
Buddy is a managed offering, meaning you don't need to actively maintain it. This is covered as part of your subscription.<p>

#### Integrations

Buddy comes prepackaged with a plethora of integrations that have been developed in house. As such, plugins don't interfere with one another and quality can be assured centrally. Deploying to AWS Lambda or to Kubernetes is a button click away. <p>
Integrations in Jenkins come in the form of plugins. Some of them have been developed in house, many more have been developed by the open source community. If an integration doesn't exist, you can always create the plugin yourself. <p>

#### Pipelines

Finally, the star of the show. With Buddy, you have two options. Using the website or writing your buddy.yaml file. You can also generate a buddy.yaml file from a UI generated pipeline. Yaml is a well understood syntax that can be externally validated. <p>
Jenkins has similar offerings. One can use the UI to create a pipeline, although this has recently fallen out of favour. It also offers pipeline as  code in the form of a Jenkinsfile. There are two options for your Jenkinsfile. Declarative and scripted. <p>

### Implementation of Buddy

For this part of the assignment we will use Buddy, so first we need to register in Buddy's web application (https://app.buddy.works/). We will do this
with our bitbucket account in order to have access to our repositories. <p>

Once again we are going to use the alternate repository as the professor suggested, the reasons being the same as before (oversized) . "https://bitbucket.org/valtersousarep/valtersousa_ca5/src/master/" <p>
Because we did not find a way to define a path into the basic application, we copied the files of the basic application into the root of this repository.<p>

So now we create a new project using the repository mentioned before "ValterSousa_CA5" and in this project we will start a new pipeline named "ca5_part2_alternative" using the repository just mentioned. 
Now for each stage of the previous Jenkinsfile we will create an action with the same purpose of each stage. In each of this stages we will use as environment "gradle 7.4.1-jdk11" because it is the gradle version we use in our application. <p>

#### Action Assemble

![buddyAssemble](buddyAssemble.png)
![buddyAssembleSucc](buddyAssembleSucc.png)

And here we can see the war file that was produced. <p>

![warFile](warFile.png)

#### Action Test

![buddyTest](buddyTest.png)
![buddyTestSucc](buddyTestSucc.png)

And as we can see, the test reports were created: <p>
![buddyTestDemo](buddyTestDemo.png)

### Action Javadoc

![buddyJavadoc](buddyJavadoc.png)
![javadocSucc](javadocSucc.png)

And here we can see the folder with the javadocs that were created: <p>

![javadocCreated](javadocFolder.png)

### Build Docker Image

For the "archiving process", we just need two actions: 
  **Build Docker Image** Using the Docker file we already created for the previous parte of this assignment <p>

![buildDocker](buildDocker.png) <p>

  **Push Docker Image** In here we use ou docker hub credentials in order for the image to be published in our account in the repository we want, in this case
in "valtersousa/ca5_part2_buddy"

![dockerPushImage](dockerPushImage.png)

### Running the pipeline

![processConclusion](processConclusion.png)

And as we can see the image we created was published in our docker hub in the repository we defined: <p>

![dockerImagePublished](dockerImagePublished.png)

