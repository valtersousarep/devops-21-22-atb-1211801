package Tests;

import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class EmployeeTest {

    @Test
    void validateStringInputsSuccessfully() {

        //Arrange
        String stringInput = "Frodo";

        //Act
        boolean result = Employee.validateStringInputs(stringInput);

        //Assert
        Assertions.assertTrue(result);
    }

    @Test
    void validateStringInputsIfNull() {

        //Arrange
        String stringInput = null;

        //Act
        boolean result = Employee.validateStringInputs(stringInput);

        //Assert
        Assertions.assertFalse(result);
    }

    @Test
    void validateStringInputsIfEmpty() {

        //Arrange
        String stringInput = "";

        //Act
        boolean result = Employee.validateStringInputs(stringInput);

        //Assert
        Assertions.assertFalse(result);
    }

    @Test
    void validateIntInputSuccessfully() {

        //Arrange
        int intInput = 0;

        //Act
        boolean result = Employee.validateIntInput(intInput);

        //Assert
        Assertions.assertTrue(result);
    }

    @Test
    void validateIntInputIfNegativeInt() {

        //Arrange
        int intInput = -5;

        //Act
        boolean result = Employee.validateIntInput(intInput);

        //Assert
        Assertions.assertFalse(result);
    }

    @Test
    void validEmployeeCreation() {

        //Arrage
        String firstName = "Frodo";
        String lastName = "Baggins";
        String description = "tester";
        String jobTitle = "tester";
        String email = "frodo@frodo.com";
        int jobYears = 50;

        //Act
        Employee employee = new Employee(firstName, lastName, description, jobTitle, jobYears, email);

        //Assert
        Assertions.assertEquals(firstName, employee.getFirstName());
        Assertions.assertEquals(lastName, employee.getLastName());
        Assertions.assertEquals(description, employee.getDescription());
        Assertions.assertEquals(jobTitle, employee.getJobTitle());
        Assertions.assertEquals(jobYears, employee.getJobYears());
        Assertions.assertEquals(email, employee.getEmail());
    }

    @Test
    void employeeCreationFails() {

        //Arrage
        String firstName = "Frodo";
        String lastName = "Baggins";
        String description = "tester";
        String jobTitle = "tester";
        String email = "frodo@frodo.com";
        int jobYears = -50;

        //Act and Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, jobYears, email));
    }

    @Test
    void validateEmailFails() {

        //Arrange
        String email = "frodo.frodo.pt";

        //Act and Assert
        Assertions.assertFalse(Employee.validateEmail(email));
    }

    @Test
    void validateEmailSuccessfully() {

        //Arrange
        String email = "frodo@frodo.pt";

        //Act and Assert
        Assertions.assertTrue(Employee.validateEmail(email));
    }
}