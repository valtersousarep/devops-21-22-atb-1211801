## CA1 - Version Control with Git

Operative Guidelines:

## First Week Assingment <p>
### 1. Clone private repository to a local folder in the local machine with the command: <p>
 
    $ git clone git clone https://ValterSousa@bitbucket.org/valtersousarep/devops-21-22-atb-1211801.git 
   Create local folder with the name **CA1**. 
   Copy code of main application already added to the repository (Tutorial React.js and Spring Data REST application) into new folder

### 2. Commit changes and push:<p>
   After creating the folder with the name **CA1** and copying the code (Tutorial React.js and Spring Data REST application) into the new folder,
   we add the changes do the repository with the following command:
    
    $ git add .  

   Then we commit the changes with a message using the following command:

    $ git commit -m "new folder CA1 created"

   And we push the changes:

    $ git push


### 3. Tagging the initial version of the application as v1.1.0
   Now we add a tag with the version of this commit "v1.1.0" and a message using the following command:

    $ git tag -a v1.1.0 -m "initial version"

   And finally pushing the changes to the repository:

    $ git push origin master v1.1.0 

### 4. Add a new field to record the years of the employee in the company (e.g., jobYears). Validate changes with unit tests.
   After the alterations to the code and the unit tests added for the creation of Employees and the validation
   of their attributes , we commit and push the changes with a new version tag v1.2.0 resolving the issue #1 and #2 that we created 
for both goals

    $ git add . 
    $ git commit -m "CA1:  add new field (jobYears) to Employee class resolving issue #2"
    $ git tag -a v1.2.0 -m "second version"
    $ git push origin master v1.2.0 

### 5. Mark end of assignament with the tag ca1-part1: <p>
    $ git tag -a ca1-part1 -m "end of first assignment"
    $ git push origin master ca1-part1

## Second Week Assignement

### 1.Developing new features in branches named after the feature. Creating a branch named "email-field" to add a new email field to the application.

Creating a new branch called "email-field  
   
     $ git branch email-field 

And then changing to that same branch to work there
    
    $ git checkout email-field 

Commit changes to the code on the branch "email-field"

    $ git add . 
    $ git commit -m "new branch email-field created, added support to email-field" 
    $ git push --set-upstream origin email-field 

After tests were added to the code, commit those changes 

    $ git add. 
    $ git commit -m "email field creation tests added" 
    $ git push --set-upstream origin email-field 

Finally merge the email-field branch to the master branch

    $ git checkout master
    $ git merge email-field 

Now adding a tag with the new version to this last commit 

    $ git tag -a v1.3.0 -m "my version 1.3.0, resolving issue #3" e9ff55f 
    $ git push origin v1.3.0

### 2. Creating branches for fixing bugs (e.g., "fix-invalid-email").

Creating new branch named fix-invalid-email. 
   
    $ git branch fix-invalid-email

Changing to the "fix-invalid-email" branch

    $ git checkout fix-invalid-email

After regex email and tests added to code, commit and push alterations to branch "fix-invalid-email regarding issue #4"

    $ git add .
    $ git commit -m ""branch fix-invalid-email created and new validation to email field created #4"
    $ git push --set-setupstream origin fix-invalid-email

Merging the branch "fix-invalid-email" to master branch

    $checkout master 
    $git merge fix-invalid-email 

### 3. Mark end of assignament with the tag ca1-part2:
Tagging the new version v1.3.1 and the repository with the tag ca1-part2

    $ git tag -a v1.3.1 -m "my version v1.3.1 resolving #4"
    $ git push --tags 
    $ git tag -a ca1-part2 -m "end of ca1-part2"
    $ git push --tags 

## Comparison Mercurial vs Git

As an alternative to Git, i chose to use Mercurial. After some research i found out that Mercurial was the most similar
alternative regarding Git. Below there is an image explaining the most important Pros and Cons of each;

![mercurialVSgit](MercurialVSGit.png)

![mercurialVSGIT](MercurialVSGit2.png)

Both are DVSC (Distributed Control Version Systems), they are quite similar and two of the most popular 
distributed control systems.
Commands in the command line for both Git and Mercurial are quite similar which made it quite simple to use both,

## Implementation of the Alternative (Mercurial)

For the implementation of Mercurial, instead of replicating the same code changes as in Git, i did a .txt file
and applied some changes to it, applying tags and branches the same way as in Git.

For this i used the command line and also TortoiseHg as a shell extension.
Below you can see in aimage of the overall experience implementation in a local repository with tortoiseHg:

![tortoise](Tortoise.png)

In this image you can see the creation of some tags ("this is my tag", "ca1TAG") and also one branch ("newBranch") which
was used to alter a .txt file as seen below, and then merged into the main branch.

![tortoise](tortoiseBranch.png)

For this i first stated my mercurial username:

    $ hg config --edit
A txt.file opens where we can input our mercurial username.

Then i changed to the directory i wanted to work and created a .txt file with a small text.

    $ cd devops-21-22-atb-1211801\CA1
    $ echo 'print("MERCURIAL TEST")' > hello.py

And now we add and commit the changes to the local repository

    $ hg add
    $ hg commit -m "mercurial test"

After this we can use 

    $ hg log

And this way we will see the commits that have been made:

![hgLOG](hgLOG.png)

After doing some changes, we can see which files have been changed and what changes have specifically been made

![hgStatus](hgStatus.png)

Then we can also add a Tag to the latest commit, and check on any tags existing in our project. Also if we do "$hg log" 
we can see the commits and the tags that were added:

![hgTag](hgTAG.png)

Now we will create a new branch to make some changes to our .txt file.
Then we will commit the changes, change to the main branch and merge the changes.

![hgBranch](hgBranch.png)

Finally we can do "$hg log" to check all the commits and the associated tags and branches we created

![hgBranch2](hgBranch2.png)




For the remote repository i used HelixTeamHub the same way we used Bitbucket for Git.
So first we create an account on HelixTeamHub and in that account a remote repository.
After that we just need to clone that repository to our Tortoise application and push all the commits we have made so 
far to the remote repository as seen below:

![cloningRep](cloningRep.png)

As we can see, all the commits were pushed into the remote repository:

![helix](helixTeamHub.png)


