## CA4 - Part 1 - Containers With Docker

The goal of this week CA is to package and execute the chat server in a container. <p>
We will create a docker image by using a dockerfile to execute the chat server. <p>
DockerFiles work very similar to vagrantFiles. We can specify the instructions we want to run in this dockerFile so the docker is built as we want it to.<p>
A Dockerfile is a text document that contains all the commands a user could call on the command line to assemble an image. Using docker build users can create an automated build that executes several command-line instructions in succession.<p>
We will do this in two different versions:

    1. In this version you should build the chat server "inside" the Dockerfile
    2. In this version you should build the chat server in your host computer and copy the jar file "into" the Dockerfile

So first we need to install "Docker Desktop", for that we need to go to the following link: <p>
https://www.docker.com/products/docker-desktop/<p>
And download the windows version of the product.

### Version 1 - In this version you should build the chat server "inside" the Dockerfile

The first step will be to create a dockerFile in order to build a docker with the chat application inside<p>

![dockerFileV1](dockerFileV1.png)

This is the dockerFile we created for this purpose <p>
First we declare the ubuntu version we want to run in the container stated in the FROM command. In this case Ubuntu:18.04. <p>
After this we have the dependencies, for example we have the commands to install git and java 8 in our docker. <p>
Then we define the command to clone our git repository and change the working directory to the folder where the chat app code is located.
We also give the permissions so we can execute the build command.<p>
Finally we set the port 59001 where we will communicate with the container, and then the runServer command. Because we use the command CMD,
this command will only run after the docker is started.<p>

Now let's build the container using the dockerFile running the following command in the terminal in the folder where the dockerfile is located. <p>

    $ docker build . -t chat_docker_v1

We needed to change the name of the docker file to "dockerfile", otherwise we get an error.<p>

![dockerBuild](dockerBuild.png)

And now let's run the following command to start the docker with the exposed port that we set in the dockerfile and the port that is set in the application code (host:guest). <p>

    $ docker run -p 59001:59001 -d chat_docker_v1

![dockerRun](dockerRun.png)
![dockerRunning](dockerRunning.png)

And as we can see the docker is up and running.

![runServer](runServer.png)

And as we set in our dockerFile, we wanted the chat server to start when the docker was started as we can see in the image above. <p>

Now let's run the client on the host machine, in the folder where the chat application is located and see if the connection with the server running in our container is working properly. <p>

    $ gradlew runClient

![runClient](runClient.png)
![serverLogs](serverLogs.png)

And as we can see, the chat app is working properly.

### Version 2 - In this version you should build the chat server in your host computer and copy the jar file "into" the Dockerfile

For this purpose we need to create a new dockerFile in a slightly different manner. Now we do not want the chat application to be
inside the docker, we want to build the chat application in our host machine and copy the jar file into the dockerfile. <p>
But first let's stop the container we have running, for this we need to get his ID first: <p>
![dockerStop](dockerStop.png)

We already built the application in our host machine, so now let's copy the far file into the same folder where we will create the dockerFile. <p>
The dockerFile will look like this: <p>

![dockerFileV2](dockerFileV2.png)
This dockerFile is simpler: <p>
First we get a docker image with the requerid dependencies from DockerHub (openjdk:17). <p>
We tried openjdk:11 first but we got the following error: <p>
![javaVersionError](javaVersionError.png)
For this reason we went for the openjdk:17. <p>
Then we set the working directory of the container to be /root. <p>
Then as said before, we copied the jar file into the container, this jar file is already in the same folder as the dockerfile. <p>
![v2Folder](v2Folder.png)
Again we expose the port 59001 and finally we run the command to start the chat server after the docker is started.

Now we run the same commands as in the first version: <p>
![runServerV2](runServerV2.png)
![runClientV2](runClientV2.png)
Now let's run the client on the host side to see if everything is working properly: <p>
        
    $ gradlew runClient

![chatWorking](chatWorking.png)

### You should tag the image and publish it in docker hub

To perform this step we created an account in docker Hub. <p>

![dockerHub](dockerHub.png)

As we can see we also created a repository in docker hub and now we can tag and publish the images that we created into this repository. <p>
So let's use the command line to login into our docker hub account using the following command:<p>

    $ docker login -u valtersousa

We also need to insert the correct password. After we log in, we must tag the image we want to push using the following command:<p>

    $ docker tag chat_docker_v1:latest valtersousa/valtersousarep_ca4_part1:chat_docker_v1

And finally pushing the image into the repository:<p>

    $ docker push valtersousa/valtersousarep_ca4_part1:chat_docker_v1

The command line display went a bit crazy but the images were pushed successfully. <p>

![imagesPushed](imagesPushed.png)
![imagesDockerHub](imagesDockerHub.png)

