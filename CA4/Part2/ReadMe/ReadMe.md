## CA4 - Part 2 - Containers With Docker

### 1. Using docker-compose to produce 2 services/containers: web and db

For this assignment we will use a docker-compose.yml file to produce two distinct services/containers:

* **web**: this container is used to run Tomcat and the spring application;
* **db**: this container is used to execute the H2 server database.<p>

For the creation of the docker-compose and the dockerfiles i used the project that the teacher made available to us with
some modifications. <p>

* **docker-compose.yml**

For the docker-compose.yml we changes the ipv4_address to the one we used with the vagrant file which was 192.168.56.10 for the web
and 192.168.56.11 for the db. Because of this we also needed to change the subnet to 192.168.56.0/24.<p>

![docker-compose-yml](docker-compose-yml.png)

* **dockerfile for web**

For the dockerfile for the web, we had to change the java version from 8 to 11 in order to work.<p>
Also we had to clone our own repository instead of the teacher and consequentially the working directory.<p>
We had some issues in running gradlew commands so we added the correct permissions to do so with "RUN chmod u+x gradlew".<p>

![dockerfile-web](dockerfile-web.png)

* **dockerfile for db**

For this dockerfile, the only change we made was the java version from 8 to 11 in order to work with our app.<p>

![dockerfile-db](dockerfile-db.png)

So after we created the dockerfiles and docker-compose file, in order to create the images using docker-compose.yml
we needed to open the terminal, change the directory to where our docker-compose.yml is located and run the following command:

    $ docker-compose build

![docker-compose-build](docker-compose-build.png)

![images](images.png)

And as we can see the images are created, now let's build the container with the following command:

    $ docker-compose up

![docker-compose-up](docker-compose-up.png)
![containers](containers.png)

Now with the container up and running let's check if the app is working properly.<p>
First let's check the web with the url http://localhost:8080/basic-0.0.1-SNAPSHOT/ <p>

![web](web.png)
As we can see is working properly. <p>
Let's now check the database with the url http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console <p>
![db](db.png)

And it's working perfectly as well. <p>

### Publishing the images in docker hub

As we done in the CA4 Part1, 
let's first log in to our docker hub account and create a new repository named "ca4_part2". <p>
After that we want to tag and push the image of the web. <p>

![dockerhub-web-commands](dockerhub-web-commands.png)

Now let's do the same to the db image.

![dockerhub-db-commands](dockerhub-db-commands.png)
![taggedAndPushed](taggedAndPushed.png)

And as we can see both images were tagges and pushed into the repository.

### Use a volume with the db container

For this part of the assignment we already added a volume in our docker-compose.yml configured for the purpose. <p>
When inside the db container, copying files into /usr/src/data-backup will copy them into our local machine to a folder named "data". <p>

![volume](volume.png)

So first let's get in the db container:

![dockerList](dockerList.png)
![insideDockerDB](insideDockerDB.png)

Now that we are inside the db container let's find the database file and copy it to the volume. <p>

![copied](copied.png)

And as expected it went to our local machine into the folder "data" as configured in the volume created with the docker-compose.yml. <p>

### 2. Alternative Solution - Kubernetes

#### **Docker**

Docker is a utility for packaging and running containers. Docker helps build standard containers that include all the 
necessary components required for them to function in isolation, including code, dependencies and libraries. 
Docker is technically more of a container management tool than a container format. <p> 
![Docker-Architecture](Docker-Architecture.png)

#### **Kubernetes**

Kubernetes is a system for operating containerized applications at scale. 
Creating these containers is the domain of Docker, and they can run anywhere, on a laptop, in the cloud, 
on local servers, and so on.<p>

A modern application consists of many containers. Operating them in production is the job of Kubernetes.
Since containers are easy to replicate, applications can auto-scale: expand or contract processing capacities 
to match user demands.<p>

![kubernetes_cluster](kubernetes_cluster.png)

#### **Kubernetes vs Docker**

Kubernetes are not really an alternative to Dockers but more of a complementary technology to Dockers.<p>

The main difference between the two is that Docker is about packaging containerized applications on a single node and
Kubernetes is meant to run them across a cluster. <p>

Of course, Docker and Kubernetes can be used independently. Whereas a large enterprise may benefit from Kubernetes 
and can support its maintenance, a smaller project may benefit from just adopting Docker. Kubernetes is most commonly 
used with Docker containers, but it can work with other container types and runtimes. <p>

![Kubernetes-vs-Docker](Kubernetes-vs-Docker.jpg)


