package Tests;

import com.greglturnquist.payroll.Employee;
import org.junit.Test;

import static junit.framework.TestCase.*;
import static org.junit.Assert.assertThrows;

public class EmployeeTest {

    @Test
    public void validateStringInputsSuccessfully() {

        //Arrange
        String stringInput = "Frodo";

        //Act
        boolean result = Employee.validateStringInputs(stringInput);

        //Assert
        assertTrue(result);
    }

    @Test
    public void validateStringInputsIfNull() {

        //Arrange
        String stringInput = null;

        //Act
        boolean result = Employee.validateStringInputs(stringInput);

        //Assert
        assertFalse(result);
    }

    @Test
    public void validateStringInputsIfEmpty() {

        //Arrange
        String stringInput = "";

        //Act
        boolean result = Employee.validateStringInputs(stringInput);

        //Assert
        assertFalse(result);
    }

    @Test
    public void validateIntInputSuccessfully() {

        //Arrange
        int intInput = 0;

        //Act
        boolean result = Employee.validateIntInput(intInput);

        //Assert
        assertTrue(result);
    }

    @Test
    public void validateIntInputIfNegativeInt() {

        //Arrange
        int intInput = -5;

        //Act
        boolean result = Employee.validateIntInput(intInput);

        //Assert
        assertFalse(result);
    }

    @Test
    public void validEmployeeCreation() {

        //Arrage
        String firstName = "Frodo";
        String lastName = "Baggins";
        String description = "tester";
        String jobTitle = "tester";
        String email = "frodo@frodo.com";
        int jobYears = 50;

        //Act
        Employee employee = new Employee(firstName, lastName, description, jobTitle, jobYears, email);

        //Assert
        assertEquals(firstName, employee.getFirstName());
        assertEquals(lastName, employee.getLastName());
        assertEquals(description, employee.getDescription());
        assertEquals(jobTitle, employee.getJobTitle());
        assertEquals(jobYears, employee.getJobYears());
        assertEquals(email, employee.getEmail());
    }

    @Test
    public void employeeCreationFails() {

        //Arrage
        String firstName = "Frodo";
        String lastName = "Baggins";
        String description = "tester";
        String jobTitle = "tester";
        String email = "frodo@frodo.com";
        int jobYears = -50;

        //Act and Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, jobYears, email));
    }

    @Test
    public void validateEmailFails() {

        //Arrange
        String email = "frodo.frodo.pt";

        //Act and Assert
        assertFalse(Employee.validateEmail(email));
    }

    @Test
    public void validateEmailSuccessfully() {

        //Arrange
        String email = "frodo@frodo.pt";

        //Act and Assert
        assertTrue(Employee.validateEmail(email));
    }
}