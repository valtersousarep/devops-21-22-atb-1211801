## CA2 - Part 1 - Build Tools With Gradle

### 1. Download and commit the professor's example application

First we will download and commit to our repository (in a folder for Part
1 of CA2) the example application available at
https://bitbucket.org/luisnogueira/gradle_basic_demo/. We will be
working with this example.

So first we will open git in the folder we want to clone the repository and then use the following command:

    $ git clone https://ValterSousa@bitbucket.org/luisnogueira/gradle_basic_demo.git

Then we add, commit and push the changes:

    $ git add .
    $ git commit -m "luisNogeuriaRep Added"
    $ git push

### 2. Experimenting the Application

So first we built the .jar file:

    % ./gradlew build 

Then we used the following command to open the chat server:

    % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

After this we opened two more terminals and used the following command to enter the chat:

    % ./gradlew runClient

![chatDemonstrationg](Chat_Demonstration.png)

### 3. Adding a new task to execute the server

To achieve this goal we will add to the build.gradle the following code:

    task runServer(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches the server"

    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatServerApp'

    args '59001'}

And now that we have the task runServer in our build.gradle using the port '59001'we can run this task through the terminal using the following command:

    ./gradlew runServer

### 4. Adding a unit test and updating the gradle script

So first we will create a test folder and the consequential folders equal to the src directory, and add a java file named "AppTest"

![AppTest](AppTest.png)

Also we will add the following dependency to our build.gradle code:

    testImplementation 'junit:junit:4.12'

![dependencie](dependencie.png)

Now we can run the tests in our terminal and check the results in our build/reports/tests/test.index

![terminalTests](terminalTests.png)
![tests](Tests.png)

### 5. Adding a new task of type Copy to be used to make a backup of the sources of the application

For this goal we will create a task named "makeBackup" of type Copy and copy all the files from the "src" folder to the "backup" folder

    task makeBackup(type: Copy){

    from 'src'
    into 'backup'

This code will be added to the build.gradle file and when we run the task from the terminal, a backup copy of our "src" folder will be created

    ./gradlew makeBackup

### 5. Adding a new task of type Zip to make a .zip archive with all sources of the application

For this goal we will add the following code to the build.gradle file:

    task makeZip (type: Zip){
    from "src"
    destinationDirectory = file("backupZip")
    archiveName("src.zip")
    }

After this task is created, we will invoke it from the terminal with the following code:

    ./gradlew makeZip

With this a zip file is created as we can see in the image below

![zipFile](zipFile.png)


